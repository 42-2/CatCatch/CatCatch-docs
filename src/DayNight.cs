﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNight : MonoBehaviour
{
	private Light sun;
	public float speed;

	private void Start()
	{
		sun = GetComponent<Light>();
	}

	private void Update()
	{
		sun.transform.Rotate(Vector3.right*speed*Time.deltaTime);
	}
}
