﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpeed : MonoBehaviour
{
    public GameObject pickupEffect;
    public float duration = 4f;
    public float multiplier = 1.4f;
    private PlayerMovement _stats;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Runner") || other.CompareTag("Catcher"))
        {
            StartCoroutine (Pickup(other));
        }
    }

    IEnumerator Pickup(Collider player)
    {
        Instantiate(pickupEffect, transform.position, transform.rotation);

        _stats = player.GetComponent<PlayerMovement>();
        _stats.Speed *= multiplier;

        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        
        
        yield return new WaitForSeconds(duration);

        _stats.Speed /= 1.4f;
        Destroy(gameObject);
    }
}
