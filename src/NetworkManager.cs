﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using System;

public class NetworkManager : MonoBehaviour
{

    private const string GameVersion = "v2.0";
    private SpawnPoint[] _spawnPoints;

	private readonly string[] _cats =
	{
		"Cedric_Cat", "Chelsea_Cat", "Edgar_Cat", "HamdiJmal_Cat", "Hervot_Cat",
		"Jakemain_Cat", "Jean_Cat", "Julien_Cat", "Marc_Cat", "Maxime_Cat",
		"Merle_Cat", "Paul_Cat", "PingPong_Cat", "Sticker_Cat", "Sylvain_Cat",
		"Tinder_Cat"
	};

	private readonly string[] _catchers =
	{
		"HamdiJmal_Cat", "Hervot_Cat", "Jakemain_Cat", "Sticker_Cat"
	};

	private readonly string[] _runners =
	{
		"Cedric_Cat", "Chelsea_Cat", "Edgar_Cat", "Jean_Cat", "Julien_Cat",
		"Marc_Cat", "Maxime_Cat", "Merle_Cat", "Paul_Cat", "PingPong_Cat",
		"Sylvain_Cat", "Tinder_Cat"
	};
	public GameObject HoldingCamera;
	public GameObject CrossHair;
	public int NumberOfPlayers;

	public bool Offline = false;
	
	// Use this for initialization
	void Start ()
	{
		_spawnPoints = FindObjectsOfType<SpawnPoint>();
		PhotonNetwork.playerName = PlayerPrefs.GetString("Username");
		PhotonNetwork.player.SetScore(0);
		NumberOfPlayers = PlayerPrefs.GetInt("NumberOfPlayers");
		Connect();
	}

	void Connect()
	{
		if (Offline)
		{
			PhotonNetwork.offlineMode = true;
			OnJoinedLobby();
		}
		else
		{
			PhotonNetwork.ConnectUsingSettings(GameVersion);
		}
	}

	private void OnGUI()
	{
		GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString()); 
	}

	void OnJoinedLobby()
	{
		Debug.Log("OnJoinedLobby");
		PhotonNetwork.JoinRandomRoom();
	}

	public static string RandomString(int length)
	{
		var random = new System.Random();
		const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		return new string(Enumerable.Repeat(chars, length)
			.Select(s => s[random.Next(s.Length)]).ToArray());
	}

	void OnPhotonRandomJoinFailed()
	{
		Debug.Log("OnPhotonRandomJoinFailed");
		var room = new RoomOptions();
		room.MaxPlayers = 8;
		var lobby = new TypedLobby();
		PhotonNetwork.CreateRoom(RandomString(8), room, lobby);
	}

	void OnJoinedRoom()
	{
		Debug.Log("OnJoinedRoom");
		Spawn();
	}
	
	// TODO: spawn players in different spawnpoints depending on their team
	void Spawn()
	{
		if (_spawnPoints == null)
		{
			Debug.Log("WTFF");
			return;
		}

		if (Offline)
		{
			SpawnPlayer();
			// if player is a catcher, pos will be greater than -1
			var pos = Array.IndexOf(_catchers, PlayerPrefs.GetString("PlayerName"));
			if (NumberOfPlayers == 4)
			{
				if(pos > -1)
					for (var i = 0; i<3;i++) SpawnPlayer(true, _runners);
				else
				{
					for (var i = 0; i<2;i++) SpawnPlayer(true, _runners);
					SpawnPlayer(true, _catchers);
				}
			}
			else
			{
				if (pos > -1)
				{
					for (var i = 0; i<6;i++) SpawnPlayer(true, _runners);
					SpawnPlayer(true, _catchers);
				}
				else
				{
					for (var i = 0; i<5;i++) SpawnPlayer(true, _runners);
					for (var i = 0; i<2;i++) SpawnPlayer(true, _catchers);
				}
			}
		}
		else
		{
			SpawnPlayer();
		}
	}

	void SpawnPlayer(bool ai = false, string[] cats = null)
	{
		// Select a random Spawnpoint to spawn the player
		var mySpawnPoint = _spawnPoints[UnityEngine.Random.Range(0, _spawnPoints.Length)];
		GameObject playerGO;
		if (!ai && PlayerPrefs.HasKey("PlayerName"))
		{
			// Instantiate the character the player chose
			playerGO = PhotonNetwork.Instantiate(PlayerPrefs.GetString("PlayerName"),
				mySpawnPoint.transform.position,
				mySpawnPoint.transform.rotation, 0);
		}
		else
		{
			// Spawn AI
			if (cats == null)
			{
				cats = _cats;
			}
			playerGO = PhotonNetwork.Instantiate(cats[UnityEngine.Random.Range(0, cats.Length)],
				mySpawnPoint.transform.position,
				mySpawnPoint.transform.rotation, 0);
		}

		// Activate the different components depending on what the player needs
		playerGO.GetComponent<Spell>().enabled = true;
        if (ai)
		{
			playerGO.GetComponent<CharacterController>().enabled = false;
			playerGO.AddComponent<Rigidbody>();
			playerGO.GetComponent<Rigidbody>().isKinematic = true;
			playerGO.GetComponent<MeshCollider>().enabled = true;
			playerGO.GetComponent<NavMeshAgent>().enabled = true;
			playerGO.GetComponent<EnnemyController>().enabled = true;
		}
		else
		{
            playerGO.GetComponent<PauseMenu>().enabled = true;
			playerGO.GetComponent<MouseLook>().enabled = true;
			playerGO.GetComponent<PlayerMovement>().enabled = true;
			HoldingCamera.SetActive(false);
			playerGO.transform.Find("Main Camera").gameObject.SetActive(true);
			playerGO.transform.Find("Main Camera").gameObject.GetComponent<MouseLook>().enabled = true;
			if (playerGO.CompareTag("Catcher"))
			{
				CrossHair.SetActive(true);
			}
			playerGO.transform.Find("SpellUI").gameObject.SetActive(true);
			playerGO.transform.Find("CanvasPauseMenu").gameObject.SetActive(true);
			playerGO.transform.Find("CanvasUserName").gameObject.SetActive(false);
		}
	}

	
	
	/*// Update is called once per frame
	void Update ()
	{
		
	}*/
}
