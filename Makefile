CDC = cdc
RAP_STN1 = rap-stn-1
PLAN_STN1 = plan-stn-1
RAP_STN2 = rap-stn-2
PLAN_STN2 = plan-stn-2
PLAN_STN_FIN = plan-stn-final
PLAN_STN_EN = plan-stn-en
RAP_PROJET = rap-projet
MANUAL = manual
COMPILE = pdflatex
PDF_VIEWER = okular

all: $(CDC).pdf $(RAP_STN1).pdf $(PLAN_STN1).pdf $(RAP_STN2).pdf $(PLAN_STN2).pdf $(RAP_PROJET).pdf $(MANUAL).pdf $(PLAN_STN_FIN).pdf $(PLAN_STN_EN).pdf

quick: # Do one single pass of pdflatex over all documents
	$(COMPILE) $(CDC).tex
	$(COMPILE) $(RAP_STN1).tex
	$(COMPILE) $(PLAN_STN1).tex
	$(COMPILE) $(RAP_STN2).tex
	$(COMPILE) $(PLAN_STN2).tex
	$(COMPILE) $(RAP_PROJET).tex
	$(COMPILE) $(MANUAL).tex
	$(COMPILE) $(PLAN_STN_FIN).tex

$(CDC).pdf: $(CDC).tex logos/*
	$(COMPILE) $(CDC).tex
	$(COMPILE) $(CDC).tex
	$(COMPILE) $(CDC).tex
	$(PDF_VIEWER) $(CDC).pdf >/dev/null 2>&1 &

$(RAP_STN1).pdf: $(RAP_STN1).tex logos/* img/*
	$(COMPILE) $(RAP_STN1).tex
	$(COMPILE) $(RAP_STN1).tex
	$(COMPILE) $(RAP_STN1).tex
	$(PDF_VIEWER) $(RAP_STN1).pdf >/dev/null 2>&1 &

$(PLAN_STN1).pdf: $(PLAN_STN1).tex
	$(COMPILE) $(PLAN_STN1).tex
	$(COMPILE) $(PLAN_STN1).tex
	$(COMPILE) $(PLAN_STN1).tex
	$(PDF_VIEWER) $(PLAN_STN1).pdf >/dev/null 2>&1 &

$(RAP_STN2).pdf: $(RAP_STN2).tex logos/* img/* pdfs/* src/*
	$(COMPILE) $(RAP_STN2).tex
	$(COMPILE) $(RAP_STN2).tex
	$(COMPILE) $(RAP_STN2).tex
	$(PDF_VIEWER) $(RAP_STN2).pdf >/dev/null 2>&1 &

$(PLAN_STN2).pdf: $(PLAN_STN2).tex
	$(COMPILE) $(PLAN_STN2).tex
	$(COMPILE) $(PLAN_STN2).tex
	$(COMPILE) $(PLAN_STN2).tex
	$(PDF_VIEWER) $(PLAN_STN2).pdf >/dev/null 2>&1 &

$(RAP_PROJET).pdf: $(RAP_PROJET).tex logos/* img/* pdfs/* src/*
	$(COMPILE) $(RAP_PROJET).tex
	$(COMPILE) $(RAP_PROJET).tex
	$(COMPILE) $(RAP_PROJET).tex
	$(PDF_VIEWER) $(RAP_PROJET).pdf >/dev/null 2>&1 &

$(MANUAL).pdf: $(MANUAL).tex logos/* img/*
	$(COMPILE) $(MANUAL).tex
	$(COMPILE) $(MANUAL).tex
	$(COMPILE) $(MANUAL).tex
	$(PDF_VIEWER) $(MANUAL).pdf >/dev/null 2>&1 &

$(PLAN_STN_FIN).pdf: $(PLAN_STN_FIN).tex
	$(COMPILE) $(PLAN_STN_FIN).tex
	$(COMPILE) $(PLAN_STN_FIN).tex
	$(COMPILE) $(PLAN_STN_FIN).tex
	$(PDF_VIEWER) $(PLAN_STN_FIN).pdf >/dev/null 2>&1 &

$(PLAN_STN_EN).pdf: $(PLAN_STN_EN).tex
	$(COMPILE) $(PLAN_STN_EN).tex
	$(COMPILE) $(PLAN_STN_EN).tex
	$(COMPILE) $(PLAN_STN_EN).tex
	$(PDF_VIEWER) $(PLAN_STN_EN).pdf >/dev/null 2>&1 &


clean:
	@rm -rvf *.out *aux *bbl *blg *log *toc *.ptb *.tod *.fls *.fdb_latexmk *.lof

distclean: clean
	@rm -rvf $(CDC).pdf $(RAP_STN1).pdf $(PLAN_STN1).pdf $(RAP_STN2).pdf $(PLAN_STN2).pdf $(RAP_PROJET).pdf $(MANUAL).pdf $(PLAN_STN_FIN).pdf $(PLAN_STN_EN).pdf

.PHONY: quick clean distclean
